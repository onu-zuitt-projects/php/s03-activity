<?php

class Person{
	
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName){

		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName(){
		return "Your full name is $this->firstName $this->middleName $this->lastName";
	}	

}

$john = new Person("John", "Matthew", "Blings");


Class Developer extends Person{

	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer";
	}
}

$emma = new Developer("Emmanuel", "Thompson", "Oliha");

Class Engineer extends Person{

	public function printName(){
		return "You are an engineer named $this->firstName $this->middleName $this->lastName";
	}
}

$obi = new Engineer("Obi", "Chukwu", "Onu");