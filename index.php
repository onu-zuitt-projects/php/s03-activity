<?php require_once "./code.php"?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S03: Activity - PHP OOP</title>
</head>
<body>
	<h1>Person</h1>
	<p><?php echo $john->printName(); ?></p>

	<h1>Developer</h1>
	<p><?php echo $emma->printName(); ?></p>

	<h1>Engineer</h1>
	<p><?php echo $obi->printName(); ?></p>

</body>
</html>